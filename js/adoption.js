$(document).ready(function(){

    $( ".pet" ).hover(
        function() {
            var inputID = $(this).find("#btn-hover input").val();
            $( this ).find("#btn-hover").append( $( "<button class=\"pet-adoptalas\" href=\"#\" value=\"" + inputID + "\">Adoptálás</button>" ) );
            $( this ).find(".hidden").append( $( "<i class=\"close material-icons\">close</i>" ) );
            $(".pet-adoptalas").click(function(){
                var number = $(this).attr("value");
                if (number.toString().length == 1) {
                    $(this).remove();
                    $("#dog" + number).find("i").remove();
                    $("#dog" + number).clone().appendTo("#done");
                    $("#wait div").remove("#dog" + number);
                } else if (number.toString().length == 2) {
                    number -= 10;
                    $(this).remove();
                    $("#cat" + number).find("i").remove();
                    $("#cat" + number).clone().appendTo("#done");
                    $("#wait div").remove("#cat" + number);
                }
            });
        }, function() {
            $( this ).find( "button:last" ).remove();
            $( this ).find( "i:last" ).remove();
        }
    );

});