$(document).ready(function(){
    //nincs szűrés, mindet kilistázza
    $("#all").click(function(){
        $('div[id^="cat"]').show();
        $('div[id^="dog"]').show();
    });
    //csak a kutyákat listázza ki
    $("#dog-filter").click(function(){
        $('div[id^="cat"]').hide();
        $('div[id^="dog"]').show();
    });
    //csak a macskákat listázza ki
    $("#cat-filter").click(function(){
        $('div[id^="dog"]').hide();
        $('div[id^="cat"]').show();
    });
});